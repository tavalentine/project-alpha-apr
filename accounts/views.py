from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from accounts.forms import LoginForm, SignUpForm


# Create your views here.
def user_login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                # return redirect("list_projects")
                return redirect("list_projects")
    else:
        form = LoginForm()
    context = {
        "form": form,
    }

    return render(request, "accounts/login.html", context)


def user_logout(request):
    logout(request)
    return redirect("login")


def signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password != password_confirmation:
                error_message = "The passwords do not match"
                context = {"form:": form, "error_message": error_message}
                return render(request, "registration/signup.html", context)

            # If passwords match, create the user
            user = User.objects.create_user(
                username=username, password=password
            )
            login(request, user)
            return redirect("list_projects")
    else:
        form = SignUpForm()
    context = {
        "form": form,
    }
    return render(request, "registration/signup.html", context)
